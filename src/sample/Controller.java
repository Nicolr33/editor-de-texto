package sample;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Controller {
    @FXML public TextArea textArea;
    @FXML public Menu editMenu;

    private int currentPx = 13;
    private File currentFile;

    @FXML public void exitEditor() {
        Platform.exit();
    }

    /**
     * Sirve para abrir el "Ayuda" de la aplicación.
     * @throws IOException
     */
    @FXML
    public void openAbout() throws IOException {
        Parent scene = FXMLLoader.load(getClass().getResource("modal.fxml"));
        Stage modal = new Stage();
        modal.setScene(new Scene(scene));
        modal.show();
    }

    /**
     * Sirve para "setear" el tamaño del texto.
     * @param ae
     */
    public void setSize(ActionEvent ae) {
        final MenuItem itemMenu = (MenuItem) ae.getSource();
        Font font = Font.getDefault();
        String pxString = itemMenu.getId().replaceAll("\\D+","");

        this.currentPx = Integer.parseInt(pxString);

        textArea.setStyle( "-fx-font: " + Integer.parseInt(pxString) + " " + font.getStyle() + ";");
    }

    /**
     * Sirve para "setear" una fuente de texto. PD: No me la cambia
     * @param ae
     */
    public void setFont(ActionEvent ae) {
        final MenuItem itemMenu = (MenuItem) ae.getSource();
        textArea.setStyle( "-fx-font: " + currentPx + " " + itemMenu.getId() + ";");
    }

    /**
     * Son las opciones del desplegable para copiar, cortar, pegar y deshacer.
     * @param ae
     */
    @FXML
    public void optionsEditMenu(ActionEvent ae) {
        final MenuItem itemMenu = (MenuItem) ae.getSource();

        switch (itemMenu.getId()) {
            case "copy":
               this.textArea.copy();
            case "cut":
                this.textArea.cut();
            case "paste":
                this.textArea.paste();
            case "undo":
                this.textArea.undo();
        }
    }

    /**
     * Sirve para guardar el fichero. Si un archivo ha sido abierto o guardado no
     * te volvera a salir la pestaña de elegir donde quieres guardar el fichero.
     */
    @FXML
    private void save() {
        if (this.currentFile == null) {
            FileChooser fileChooser = new FileChooser();

            //Set extension filter for text files
            FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
            fileChooser.getExtensionFilters().add(extFilter);

            //Show save file dialog
            File file = fileChooser.showSaveDialog(new Stage());

            if (file != null) {
                saveTextToFile(file);
            }
        } else {
            saveTextToFile(this.currentFile);
        }

    }

    /**
     * Es la función que se encarga de guardar el fichero
     * @param file
     */
    private void saveTextToFile(File file) {
        try {
            PrintWriter writer;
            writer = new PrintWriter(file);
            writer.println(this.textArea.getText());
            writer.close();
            this.currentFile = file;
        } catch (IOException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Sirve para abrir el fichero y mostrarlo en el editor de texto. Tambien cambia el title de la ventana al nombre de fichero.
     * @throws FileNotFoundException
     */
    @FXML
    private void openFile() throws FileNotFoundException {
        FileChooser fileChooser = new FileChooser();

        //Set extension filter for text files
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("TXT files (*.txt)", "*.txt");
        fileChooser.getExtensionFilters().add(extFilter);

        //Show save file dialog
        File file = fileChooser.showOpenDialog(new Stage());

        if (file != null) {
            Main.getStage().setTitle(file.getName() + " - Editor de texto");
            openTextFile(file);
        }
    }

    /**
     * Es la funcion que se encarga de leer el fichero y escribirlo en el editor.
     * @param file
     * @throws FileNotFoundException
     */
    private void openTextFile(File file) throws FileNotFoundException {
        BufferedReader br = new BufferedReader(new FileReader(file));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();
            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            this.textArea.setText(sb.toString());
            this.currentFile = file;
        } catch (IOException ex) {
            Logger.getLogger(Controller.class.getName()).log(Level.SEVERE, null, ex);
        }
    }





}
